# Laravel 5.4


----------


## Note Pad
Listing storing updating and deleting a note. I have created both apps. For RestFUL API support and templating based application.

 - Add new note 
 - Update existing note 
 - Get all note 
 - Delete a note

## Added API support
API Method

 - GET 	 http://127.0.0.1:8000/api/note 
 - POST http://127.0.0.1:8000/api/note 
 - PUT   http://127.0.0.1:8000/api/note/15 
 - DELETE http://127.0.0.1:8000/api/note/11

Param 

 - title : required 
 - description : required

## Installation
- Clone this repo
- Run : `composer update`
- Change DataBase configure
- Run : `php artisan migrate`
- Start local server : `php artisan serve`
- Go to link : http://127.0.0.1:8000/note

### Todos

 - Google Calender Integration
 - User Auth
 - Category
 - File attachment to note

### Screen shot
![Alt text](https://preview.ibb.co/gaChDF/Notes_demo.png)

License
----
MIT