<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Validator;
use App\Note;

class NotesController extends Controller
{

    public function getNotes($itemsPerPage = 5)
    {   
        // using paginate function to show 5 Note items per page    
        $notes = Note::orderBy('id', 'desc')->paginate($itemsPerPage);
        return $notes;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {           
        $notes = $this->getNotes();
        return view('notes.index', array('notes' => $notes, 'button' => 'Submit'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return redirect('note')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        Note::create($input);
        
        Session::flash('flash_message', 'Note added successfully!');
        return redirect()->route('note.index');
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::findOrFail($id);
        $notes = $this->getNotes();
        return view('notes.index', array('note' => $note, 'notes' => $notes,'button' => 'Update'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $note = Note::findOrFail($id);
 
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return redirect('note/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
 
        $note->fill($input)->save();
 
        Session::flash('flash_message', 'Note updated successfully!');
        return redirect()->route('note.index');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);
 
        $note->delete();
 
        Session::flash('flash_message_delete', 'Note deleted successfully!');
 
        return redirect()->route('note.index');
    }
}
