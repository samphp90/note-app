<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Note;
use Illuminate\Http\Response;

class NotesController extends Controller
{

    public function getNotes($itemsPerPage = 5)
    {   
        // using paginate function to show 5 Note items per page    
        $notes = Note::orderBy('id', 'desc')->paginate($itemsPerPage);
        return $notes;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {           
        $notes = $this->getNotes();
        return response()->json($notes);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            $data = array('status' => 'fail','success' => false, 'date' => array('Title or description not valid'), 'message' => 'Note not added!');
            return response()->json($data);
        }
        
        Note::create($input);
        $data = array('status' => 'success','success' => true, 'date' => array(), 'message' => 'Note added successfully!');

        return response()->json($data);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $note = Note::findOrFail($id);
 
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            $data = array('status' => 'fail','success' => false, 'date' => array('Title or description not valid'), 'message' => 'Note not updated!');
            return response()->json($data);
        }
 
        $note->fill($input)->save();
        $data = array('status' => 'success','success' => true, 'date' => array(), 'message' => 'Note updated successfully!');

        return response()->json($data);
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);
 
        $note->delete();
 
        $data = array('status' => 'success','success' => true, 'date' => array(), 'message' => 'Note deleted successfully!');

        return response()->json($data);
    }
}
